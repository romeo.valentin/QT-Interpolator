#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <memory>
#include "qcustomplot.h"
#include "spline.h"

namespace Ui {
class MainWindow;
}

struct DataSet {
    double x_lower, x_upper, n;
    std::vector<double> x_ls;
    DataSet();
    DataSet(double x_l, double x_u, int n);
    std::vector<double> xs, ys;
    void addPoint(double x, double y);
    QSharedPointer<QCPGraphDataContainer> generateData(const std::vector<double> &xs);
    tk::spline spline;
    void setY(double& y);
    double lagrange(double x);
    double l_i(unsigned i, double x);
};

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void setupBasicPlot(QCustomPlot *customPlot);
    void setupMousePlot(QCustomPlot *customPlot);


private slots:
    void mousePress();
    void handleClearButtonPress();

private:
    Ui::MainWindow* ui;
    QString name;
    DataSet points;
    QCPScatterStyle data;
    QCPScatterStyle interpolation;
};


#endif // MAINWINDOW_H
