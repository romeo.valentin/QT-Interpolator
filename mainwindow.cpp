#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <vector>
#include <algorithm>
#include <cmath>
using std::vector;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    connect(ui->customPlot, SIGNAL(mousePress(QMouseEvent*)), this, SLOT(mousePress()));
    connect(ui->clear_button, SIGNAL (clicked()), this, SLOT (handleClearButtonPress()));
    //setGeometry(400, 250, 800, 800);
    //setupBasicPlot(ui->customPlot);
    setupMousePlot(ui->customPlot);
    setWindowTitle("Cubic Spline Interpolator");
    statusBar()->clearMessage();
    ui->customPlot->replot();
}

MainWindow::~MainWindow()
{
    delete ui;
}

DataSet::DataSet()
{
    ;
}

DataSet::DataSet(double x_l, double x_u, int n) :
    x_lower(x_l), x_upper(x_u), n(n)
{
    x_ls = std::vector<double>(n);
    std::for_each(x_ls.begin(), x_ls.end(), [&](double& x_){static double x = x_lower; x_ = x; x += (x_upper-x_lower)/(n-1);});
}

void DataSet::addPoint(double x, double y)
{
    if (std::find(xs.begin(), xs.end(), x) != xs.end())
        return;
    unsigned i = 0;
    while (i < xs.size() && x > xs[i])
        i++;
    xs.insert(xs.begin()+i, x);
    ys.insert(ys.begin()+i, y);
    if (xs.size() > 2)
    spline.set_points(xs,ys);
}

double DataSet::lagrange(double x)
{
    double y = 0;
    for (unsigned i = 0; i < xs.size(); i++) {
        y += ys[i]*l_i(i, x);
    }
    return y;
}
double DataSet::l_i(unsigned i, double x)
{
    double res = 1;
    for (unsigned j = 0; j < xs.size(); j++) {
        if (j != i)
        res *= (x - xs[j])/(xs[i]-xs[j]);
    }
    return res;
}

QSharedPointer<QCPGraphDataContainer> DataSet::generateData(const std::vector<double> &xs)
{
    std::vector<double> ys(xs);
    std::for_each(ys.begin(), ys.end(), [&](double& y) { y = spline(y);});
    QSharedPointer<QCPGraphDataContainer> plot_data = QSharedPointer<QCPGraphDataContainer>(new QCPGraphDataContainer);
    for (unsigned i = 0; i < xs.size(); i++) {
        plot_data->add(QCPGraphData(xs[i], ys[i]));
    }

    return plot_data;
}


void MainWindow::setupMousePlot(QCustomPlot *customPlot)
{
    name = "Mouse plot";
    points = DataSet(0,1,100);
    customPlot->addGraph();
    // set axes labels
    customPlot->xAxis->setLabel("x");
    customPlot->yAxis->setLabel("y");
    // set axes ranges, so we see all data:
    customPlot->xAxis->setRange(points.x_lower, points.x_upper);
    customPlot->yAxis->setRange(0, 1);
    //customPlot->graph()->setLineStyle(QCPGraph::LineStyle::lsNone);
    data = QCPScatterStyle(QCPScatterStyle::ssCircle, Qt::red, Qt::white, 5);
    interpolation = QCPScatterStyle(QCPScatterStyle::ssCircle, Qt::blue, Qt::white, 1);
}

void MainWindow::mousePress()
{
    QPoint mousePos = ui->customPlot->mapFromGlobal(QCursor::pos());
    //double scaled_x = (double(mousePos.x()) / ui->customPlot->width());
    //double scaled_y = (1 - double(mousePos.y()) / ui->customPlot->height());
    double scaled_x, scaled_y;
    ui->customPlot->graph(0)->pixelsToCoords(mousePos, scaled_x, scaled_y);
    if (scaled_x >= points.x_lower && scaled_x <= points.x_upper) {
        points.addPoint(scaled_x,scaled_y);
        //ui->customPlot->graph(0)->addData(scaled_x, scaled_y);
        //ui->customPlot->replot();
        if (points.xs.size() > 2) {
          ui->customPlot->graph()->setScatterStyle(interpolation);
          ui->customPlot->graph()->setData(points.generateData(points.x_ls));
          ui->customPlot->graph()->setScatterStyle(data);
          ui->customPlot->graph()->addData(QVector<double>::fromStdVector(points.xs), QVector<double>::fromStdVector(points.ys));
        }
        else {
          ui->customPlot->graph()->setScatterStyle(data);
          ui->customPlot->graph()->addData(scaled_x, scaled_y);
        }
        ui->customPlot->replot();
    }
}

void MainWindow::handleClearButtonPress()
{
    points.xs.clear();
    points.ys.clear();
    ui->customPlot->graph()->setData(QVector<double>(), QVector<double>());
    ui->customPlot->replot();

}
