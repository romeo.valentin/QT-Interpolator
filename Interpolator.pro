#-------------------------------------------------
#
# Project created by QtCreator 2017-05-17T22:06:44
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport

TARGET = Interpolator
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
        qcustomplot.cpp

HEADERS  += mainwindow.h \
         qcustomplot.h \
         spline.h

FORMS    += mainwindow.ui

CONFIG += c++14
